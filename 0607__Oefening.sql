USE modernways;

SELECT t.Omschrijving
FROM taken t
	LEFT JOIN leden l ON t.Leden_Id = l.Id
WHERE  t.Leden_Id IS NULL;