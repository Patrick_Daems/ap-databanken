USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSuccess`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleaseWithSuccess` (OUT success BOOL)
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    SELECT count(*) INTO numberOfAlbums FROM bands;
    SELECT count(*) INTO numberOfBands FROM bands;
    SELECT FLOOR(RAND() * numberOfAlbums) + 1 INTO randomAlbumId;
	SELECT FLOOR(RAND() * numberOfBands) + 1 INTO randomBandId;
    IF NOT EXISTS(SELECT * FROM albumreleases WHERE Bands_Id = randomBandId AND Albums_Id = randomAlbumId) THEN
		INSERT INTO albumreleases (Bands_Id,Albums_Id) VALUES (randomBandId,randomAlbumId);
        SELECT 1 INTO success;
	ELSE
		SELECT 0 INTO success;
	END IF;
END $$
DELIMITER ;