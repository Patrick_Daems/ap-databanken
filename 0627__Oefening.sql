USE modernways;

SELECT Voornaam
FROM studenten
WHERE length(Voornaam) <(SELECT AVG(length(Voornaam)) FROM studenten);