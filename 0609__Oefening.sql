USE modernways;

SELECT g.Titel , COALESCE(p.Naam, "Niet meer ondersteund") AS Naam
FROM games g
	LEFT JOIN releases r ON g.Id = r.Games_Id
    LEFT JOIN platformen p ON r.Platformen_Id = p.Id;