USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`;
CREATE PROCEDURE `NumberOfGenres`(OUT amount TINYINT)
BEGIN
	SELECT COUNT(*)
    INTO amount
    FROM genres;
END
DELIMITER ;