USE modernways;

SELECT l.Voornaam, t.Omschrijving
FROM taken t
	LEFT JOIN leden l ON t.Leden_Id = l.Id;