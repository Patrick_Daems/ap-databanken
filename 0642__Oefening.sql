USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN cleanDate DATE, OUT amountCleaned INT)
BEGIN
	DECLARE membersBefore INT;
    DECLARE membersAfter INT;
    SELECT count(*) INTO membersBefore FROM aptunes.lidmaatschappen;
    SET SQL_SAFE_UPDATES = 0;
	DELETE FROM lidmaatschappen WHERE Einddatum < cleanDate;
	SET SQL_SAFE_UPDATES = 1;
    SELECT count(*) INTO membersAfter FROM aptunes.lidmaatschappen;
    SELECT membersBefore - membersAfter INTO amountCleaned;
END $$
DELIMITER ;
