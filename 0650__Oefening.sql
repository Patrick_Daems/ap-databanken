USE `aptunes`;
DROP PROCEDURE IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
	-- variabelen declareren
	DECLARE random INT DEFAULT 0;
    DECLARE teller INT DEFAULT 0;
    DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    -- De error handler met exit aanmaken	
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		ROLLBACK;
        SELECT 'Nieuwe releases konden niet worden toegevoegd.' message;
    END;
    START TRANSACTION;
    -- De lus met een While
		WHILE teller < 3 DO
			SELECT count(*) INTO numberOfAlbums FROM bands; -- aantal albums
			SELECT count(*) INTO numberOfBands FROM bands; -- aantal bands
			SELECT FLOOR(RAND() * numberOfAlbums) + 1 INTO randomAlbumId; -- willekeurig album
			SELECT FLOOR(RAND() * numberOfBands) + 1 INTO randomBandId; -- willekeurige band
			INSERT INTO albumreleases (Bands_Id,Albums_Id) VALUES (randomBandId,randomAlbumId);
			-- vanaf de 2 insert een random genereren
			IF teller >= 2 THEN
				SET random = FLOOR(RAND() * 3) + 1;
				-- indien het nummer 1 is error throwen
				IF random = 1 THEN
					SIGNAL SQLSTATE '45000';
				END IF;   
			END IF;
			SET teller = teller +1;
		END WHILE;
    COMMIT;
END$$
DELIMITER ;