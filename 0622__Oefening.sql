USE aptunes;

CREATE INDEX VoornaamFamilienaamIdx
ON muzikanten(Voornaam(9), Familienaam(9));