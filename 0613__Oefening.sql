USE modernways;


CREATE VIEW AuteursBoeken
AS
SELECT CONCAT(p.Voornaam , ' ' , p.Familienaam) as Auteur, b.Titel
FROM publicaties pub 
INNER JOIN personen p  on p.Id = pub.Personen_Id
INNER JOIN boeken b on pub.boeken_Id = b.id
;
