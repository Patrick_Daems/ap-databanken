USE modernways;

SELECT s.Id
FROM evaluaties e
INNER JOIN studenten s On e.Studenten_Id = s.Id
GROUP BY s.Id
HAVING  avg(e.cijfer)>(SELECT avg(cijfer) as algGemiddeldeCijfer 
FROM evaluaties);