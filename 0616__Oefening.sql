USE modernways;

CREATE VIEW GemiddeldeRatings
AS
SELECT Boeken_Id, avg(Rating) 
FROM reviews
GROUP BY Boeken_Id;