USE modernways;

SELECT Naam
FROM platformen p
	LEFT JOIN releases r ON p.Id = r.Platformen_Id
WHERE r.Platformen_Id IS NULL;