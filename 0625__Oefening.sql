USE aptunes;

CREATE INDEX  FamilienaamVoornaamIdx
ON muzikanten(Familienaam, Voornaam);
