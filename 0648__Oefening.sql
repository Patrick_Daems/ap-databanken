USE `aptunes`;
DROP PROCEDURE IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
	DECLARE random INT DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'  SELECT 'State 45002 opgevangen. Geen probleem.' message;	
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION  SELECT 'Een algemene fout opgevangen.' message;
    
    SET random = FLOOR(RAND() * 3) + 1;
    IF random = 1 THEN
		SIGNAL SQLSTATE '45001';
    ELSEIF random = 2 THEN
		SIGNAL SQLSTATE '45002';
    ELSE
		SIGNAL SQLSTATE '45003';
    END IF;    
END$$
DELIMITER ;