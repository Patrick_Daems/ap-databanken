USE modernways;

CREATE VIEW AuteursBoekenRating
AS
SELECT ab.Auteur, ab.Titel, gr.Rating
FROM auteursboeken AS ab
INNER JOIN gemiddelderatings gr ON ab.Boeken_ID = gr.Boeken_Id;