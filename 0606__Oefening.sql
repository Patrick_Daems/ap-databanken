USE modernways;

SELECT l.Voornaam, b.Titel 
FROM uitleningen u
	INNER JOIN leden l ON u.Leden_Id = l.Id
    INNER JOIN boeken b ON u.Boeken_Id = b.Id;