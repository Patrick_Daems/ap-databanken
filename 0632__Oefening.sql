USE modernways;

SELECT Voornaam, Familienaam 
FROM directieleden
WHERE Loon >=ALL(
		SELECT Loon FROM directieleden
		UNION
		SELECT Loon FROM personeelsleden);