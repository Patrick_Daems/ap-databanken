USE modernways;

SELECT DISTINCT Voornaam
FROM studenten
WHERE Voornaam in (SELECT Voornaam 
					FROM directieleden 
                    WHERE Voornaam in 
							(SELECT Voornaam FROM personeelsleden));