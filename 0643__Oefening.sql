USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum` (IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
	START TRANSACTION;
	INSERT INTO albums(titel) VALUES(titel);
    INSERT INTO albumreleases(Bands_id,Albums_Id) VALUES(bands_id, LAST_INSERT_ID());
    COMMIT;
END $$
DELIMITER ;