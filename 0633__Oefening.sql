USE modernways;

SELECT Voornaam, Familienaam 
FROM directieleden
WHERE Loon <ANY(
		SELECT Loon FROM directieleden
		UNION
		SELECT Loon FROM personeelsleden);