USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT 0;
    
    loop_addReleases: LOOP
		IF counter >= extraReleases THEN
			LEAVE loop_addReleases;
        ELSE    
			CALL MockAlbumReleaseWithSuccess(@success);
			IF @Success = 1 then
				SET counter = counter +1;
			END IF;
		END IF;
    END LOOP;
    
END $$
DELIMITER ;