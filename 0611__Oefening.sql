USE modernways;

SELECT g.Titel  , 'Geen platformen gekend' as Naam
FROM releases r 
    RIGHT JOIN games g ON r.Games_Id = g.Id
WHERE  r.Games_ID IS NULL
UNION ALL
SELECT 'Geen games gekend' AS Titel  ,Naam
FROM platformen p
	LEFT JOIN releases r ON p.Id = r.Platformen_Id    
WHERE r.Platformen_Id IS NULL;