USE `aptunes`;
DROP procedure IF EXISTS `getLiedjes`;

DELIMITER $$
USE `aptunes`;
CREATE PROCEDURE `getLiedjes`(IN bevatWoord VARCHAR(50) )
BEGIN
	SELECT Titel
    FROM liedjes
    WHERE Titel LIKE concat('%', bevatWoord ,'%'); 
END
DELIMITER ;