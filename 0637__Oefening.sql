-- Tabel personen aanmaken
use ModernWays;
drop table if exists Personen;
create table Personen (
	Id int auto_increment primary key,
	Voornaam varchar(255) not null,
	Familienaam varchar(255) not null,    
	AanspreekTitel varchar(30) null,
	Straat varchar(80) null,
	Huisnummer varchar (5) null,
	Stad varchar (50) null,
	Commentaar varchar (100) null,
	Biografie varchar(400) null
);
-- De gegevens van de auteurs (naam, voornaam) in de personentabel toevoegen
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
-- aan de tabel boeken de kolom toevoegen voor de id van personen
alter table Boeken add Personen_Id int null;

-- De Id's van de personen  opvullen
-- Eerst de sql_safe_uodates op 0 zetten en nadien weer op 1 plaatsen
SET SQL_SAFE_UPDATES = 0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;
-- De kolommen voornaam en naam weg doen
alter table Boeken drop column Voornaam,
    drop column Familienaam;
-- De Foreign key toevoegen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);