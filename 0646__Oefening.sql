USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleases` (IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT 0;
    
    REPEAT
		CALL MockAlbumReleaseWithSuccess(@success);
        IF @Success = 1 then
			SET counter = counter +1;
        END IF;
    UNTIL counter >= extraReleases
    END REPEAT;
    
END $$
DELIMITER ;